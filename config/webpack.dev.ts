import merge from 'webpack-merge'
import { HotModuleReplacementPlugin } from 'webpack'
import type { Configuration } from 'webpack'

import common from './webpack.common'
import { CONFIG } from './constants'


export default merge(common, {
  mode: 'development',
  target: 'web',
  devtool: 'source-map',
  output: {
    filename: '[name].js',
    publicPath: '/',
  },
  optimization: {
    minimize: false,
    splitChunks: false,
  },
  devServer:  {
    host: CONFIG.HOST,
    port: CONFIG.PORT,
    contentBase: './public',
    historyApiFallback: true,
    disableHostCheck: true,   
    noInfo: true,  
  },
  plugins: [
    new HotModuleReplacementPlugin(),
    
  ]
  

} as Configuration)
