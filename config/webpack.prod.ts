import path from 'path'
import merge from 'webpack-merge'
import type { Configuration} from 'webpack'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import TerserPlugin from 'terser-webpack-plugin'
import CopyPlugin from 'copy-webpack-plugin'

import common from './webpack.common'
import { PATH } from './constants'

export default merge(common, {
  mode: 'production',
  target: 'browserslist',
  devtool: false,
  output: {
    path: path.resolve(PATH.PROJECT_PATH, './public'),
    filename: 'js/[name].[contenthash:8].js',
    assetModuleFilename: 'static/[name].[contenthash:8].[ext]',
  },
  optimization: {
    minimize: true,
    minimizer:[
      new TerserPlugin({
        extractComments: false,
        terserOptions: {
          compress: { pure_funcs: ['console.log'] },
        }
      }),
    ]
  },

  plugins: [
    new MiniCssExtractPlugin({
      filename: 'css/[name].[contenthash:8].css',
      chunkFilename: 'css/[name].[contenthash:8].chunk.css',
    }),
    // new CopyPlugin({
    //   patterns: [
    //     {
    //       context: path.resolve(PATH.PROJECT_PATH, './resources'), 
    //       from: '**',
    //       to: path.resolve(PATH.PROJECT_PATH, './public/resources'), 
    //       toType: 'dir',
    //       globOptions: {
    //         dot: true,
    //         gitignore: true,
    //         ignore: ['**/index.html'],		
    //       },
    //     },
    //   ],
    // }),
  ]
  

} as Configuration)
