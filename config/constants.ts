import path from 'path'

const CONFIG = {
  HOST: process.env.HOST || '0.0.0.0',
  PORT: process.env.PORT || 80,
}

const PATH = {
  PROJECT_PATH: path.resolve(__dirname, '../'),
}


export {
  CONFIG,
  PATH
}
