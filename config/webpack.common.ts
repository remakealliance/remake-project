import path from 'path'
import type { Configuration } from 'webpack'
import WebpackBar from 'webpackbar'
import HtmlWebpackPlugin from 'html-webpack-plugin'
import { CleanWebpackPlugin } from 'clean-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin'
import CopyPlugin from 'copy-webpack-plugin'


import { PATH } from './constants'

const isDev = process.env.NODE_ENV === 'development'
const isProd = process.env.NODE_ENV === 'production'

// 待完成：css配置
const getExtractCssLoader = () => {
  const ExtractCssLoader: any = [
    isDev ? 'style-loader' : MiniCssExtractPlugin.loader,
    {
      loader: 'css-loader',
      options: {
        modules: false,
        sourceMap: isDev,
      }
    }
  ]
  isProd && ExtractCssLoader.push({
    loader: 'postcss-loader',
    options: {
      postcssOptions: {
        plugins: [[
          'postcss-preset-env',
          { autoprefixer: { grid: true } }
        ]]
      }
    }
  })
  return ExtractCssLoader
}


export default {
  entry: {
    app: path.resolve(PATH.PROJECT_PATH, './src/index.tsx') 
  },
  resolve: {
    extensions: ['.tsx', '.ts', '.js', '.json'],
    alias: {
      '@': path.resolve(PATH.PROJECT_PATH, './src'),
    }
  },
  cache: {
    type: 'filesystem',
    buildDependencies: {
      config: [__filename],
    },
  },
  module: {
    rules: [
      {
        test: /\.(tsx?|js)$/,
        loader: 'babel-loader',
        options: { cacheDirectory: true },
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: getExtractCssLoader(),
      },
      {
        test: /\.scss$/,
        use: [
          ...getExtractCssLoader(),
          {
            loader: 'sass-loader',
            options: {
              sourceMap: isDev,
            }
          },

        ],
      },
      {
        test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 4 * 1024,
          },
        },
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2?)$/,
        type: 'asset/resource',
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(PATH.PROJECT_PATH, './src/index.html')
    }),
    new ForkTsCheckerWebpackPlugin({
      typescript: {
        configFile: path.resolve(PATH.PROJECT_PATH, './tsconfig.json'),
      },
    }),
    new CleanWebpackPlugin(),
    new WebpackBar({
      name: 'Link Startou!!!', 
      color: '#52c41a' 
    }),
    new CopyPlugin({
      patterns: [
        {
          context: 'resources', 
          from: '*',
          to: path.resolve(PATH.PROJECT_PATH, './public/resources'), 
          toType: 'dir',
          globOptions: {
            dot: true,
            gitignore: true,
            ignore: ['**/index.html'],		
          },
        },
      ],
    }),
  ]

} as Configuration


